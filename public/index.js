//IIFE
(function(){
    var MyApp = angular.module("MyApp", []);
    MyCtrl = function ($http) {
        var myCtrl = this;
        //freshClone creates an object that contains only
        //the neccessary info needed to be passed to the
        //server
        myCtrl.freshClone = ()=>{
            // var newObj = JSON.parse(JSON.stringify(myCtrl))
            // console.log(newObj);
            //delete newObj["passwordErrorMsg"]
            //delete newObj["result"]
            return {
                email:myCtrl.email,
                password:myCtrl.password,
                name:myCtrl.name,
                gender:myCtrl.gender,
                date:myCtrl.date,
                address:myCtrl.address,
                country:myCtrl.address,
                contact:myCtrl.contact
            }
        }
        myCtrl.init = () => {
            myCtrl.email = "";
            myCtrl.password = "";
            myCtrl.name = "";
            myCtrl.gender = "male";
            myCtrl.date = "";
            myCtrl.address = "";
            myCtrl.country = "";
            myCtrl.contact = "";

            myCtrl.passwordErrMsg = "";
            myCtrl.result = "";
        }
        myCtrl.init();
    
        myCtrl.submitForm = () => {
            var promise = $http.get("/register", {
                params: {
                    userInfo: myCtrl.freshClone()
                }
            }).then((result) => {
                myCtrl.result = result.data;
            }).catch((error) => {
                myCtrl.result = error.data;
            });
        }

        myCtrl.isValidPassword = () => {
            // Password must adhere to the following strong password policies:
            // i. have at least 8 characters
            // ii. use both uppercase and lowercase letters (case sensitivity)
            // iii. include one or more numerical digits
            // iv. include one of these special characters @, #, $  

            // console.log("password check");
            if (myCtrl.password == null) {
                myCtrl.passwordErrMsg = "Password have at least 8 characters\n";
                return false;
            }
            if (myCtrl.password != null && myCtrl.password.length < 8) {
                // console.log("len too short")
                myCtrl.passwordErrMsg = "Password have at least 8 characters\n";
                return false;
            }
            if (!(/(?=.*[a-z])(?=.*[A-Z])/.test(myCtrl.password))) {
                myCtrl.passwordErrMsg = "Password must use both uppercase and lowercase letters\n";
                return false;
            }
            if (!(/[0-9]/.test(myCtrl.password))) {
                myCtrl.passwordErrMsg = "Password must have one or more numerical digits\n"
                return false;
            }
            if (!(/([@$#])/.test(myCtrl.password))) {
                myCtrl.passwordErrMsg = "Password must include one of these special characters @, #, $\n"
                return false;
            }
            return true;
        }

        myCtrl.isValidAge = () => {
            var dt2 = new Date();
            var dt1 = new Date(myCtrl.date);
            // var ageDate = (dt2.getTime()-dt1.getTime());
            var diff = (dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24);
            // console.log("diff is " + diff);
            if (diff < 0 || isNaN(diff)) {
                // console.log("negative years");
                return false;
            }
            var calAge = Math.floor(diff / 365.25);
            // console.log("calAge: " + calAge);
            if (calAge < 18 || calAge > 120) {
                return false;
            }
            return true;
        }

        myCtrl.isValidContact = () => {
            //restricting contact number to maximum of 20 characters
            return /^[\s()+-]*([0-9][\s()+-]*){0,20}$/.test(myCtrl.contact);
        }
    }
    MyCtrl.$inject = ["$http"];
    MyApp.controller("MyCtrl", MyCtrl)
})();