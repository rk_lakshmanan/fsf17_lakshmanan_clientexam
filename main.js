//Load Library
var express = require('express');

//Create application
var app = express();

//Routing
app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));

app.get("/register", (req, res) => {
    console.log(req.query.userInfo);
    res.status(202);
    res.type("text/plain");
    res.send("Thank you");
})

//Start app on port
var port = process.argv[2] || 3000;
app.listen(port, () => {
    console.log("App running on port " + port);
})